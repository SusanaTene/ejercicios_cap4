print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 6 del capitulo 5.')
print('Autora: Lilia Susana Tene. Mail: lilia.tene@unl.edu.ec')

#Ejercicio 6: Reescribe el programa de cálculo del salario, con tarifa-y-media
#para las horas extras, y crea una función llamada calculo_salario
#que reciba dos parámetros (horas y tarifa).

def calculo_salario(horas, tarifa):
    if horas <= 40:
        salario = (horas * tarifa)
    if horas > 40:
        hextras = horas - 40
        salario = (40 * tarifa) + (hextras * (tarifa/2+tarifa))
    return salario

print ('Introduzca horas trabajadas:')
h = float(input())
print ('Introduzca la tarifa y media por hora:')
t = float(input())
salario = calculo_salario(h, t)
print("Su salario es:", salario)


