print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 2 del capitulo 5.')
print('Autora: Lilia Susana Tene. Mail: lilia.tene@unl.edu.ec')

#Ejercicio 2: Desplaza la última línea del programa anterior hacia arriba,
#de modo que la llamada a la función aparezca antes que las definiciones.
#Ejecuta el programa y observa qué mensaje de error obtienes.

repite_estribillo()

def muestra_estribillo():
    print('Soy un leñador, que alegría.')
    print('Duermo toda la noche y trabajo todo el día.')

def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()

#Al ejecutar en programa nos muestra el siguiente error "" name 'repite_estribillo' is not defined""
# que nos dice que el nombre que tenemos como repite estribillo no se encuentra definido.


