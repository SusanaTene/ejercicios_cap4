print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 7 del capitulo 5')
print('Autora: Lilia Susana Tene. Mail: lilia.tene@unl.edu.ec')

#Ejercicio 7: Reescribe el programa de calificaciones del capítulo anterior
#usando una función llamada calcula_calificacion, que reciba una
#puntuación como parámetro y devuelva una calificación como cadena.

def calculo_calificacion(puntuacion):

    if puntuacion >= 0 and puntuacion <= 1.0:
        if puntuacion >= 0.9:
            print('Sobresaliente')
        elif puntuacion >= 0.8:
            print('Notable')
        elif puntuacion >= 0.7:
            print('Bien')
        elif puntuacion >= 0.6:
            print('Suficiente')
        elif puntuacion < 0.6:
            print('Insuficiente')
    else:
        print('puntuacion incorrecta')
    return puntuacion

try:
    p = float(input('Introduzca puntuacion='))
        # validacion del rango de la puntuacion
    calculo_calificacion(p)
except:
    print('Puntuacion Incorrecta')
