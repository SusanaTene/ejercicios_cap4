print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 4 del capitulo 5.')
print('Autora: Lilia Susana Tene. Mail: lilia.tene@unl.edu.ec')

#Ejercicio 4: ¿Cuál es la utilidad de la palabra clave “def” en Python?
#a) Es una jerga que significa “este código es realmente estupendo”
#b) Indica el comienzo de una función
#c) Indica que la siguiente sección de código indentado debe ser almacenada para
#usarla más tarde
#d) b y c son correctas ambas
#e) Ninguna de las anteriores

print ("-+-+-+-+-+-+-+-TEST-+-+-+-+-+-+-+-+-")
print ("Responda la siguiente pregunta de selección multiple")
print ("¿Cuál es la utilidad de la palabra clave “def” en Python?")
print ("a) Es una jerga que significa “este código es realmente estupendo”.")
print ("b) Indica el comienzo de una función")
print ("c) Indica que la siguiente sección de código indentado debe ser almacenada para usarla más tarde")
print ("d) b y c son correctas ambas")
print ("e) Ninguna de las anteriores")

print ("Escriba su respuesta:")
letra = input()
if letra == 'b':
    print ("Respuesta Correcta:", letra, ": Indica el comienzo de una función.")
else:
    print("¡No me lo creo!")