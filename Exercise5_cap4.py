print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 5 del capitulo 5.')
print('Autora: Lilia Susana Tene. Mail: lilia.tene@unl.edu.ec')

#Ejercicio 5: ¿Qué mostrará en pantalla el siguiente programa Python?
print("-+-+-+-Pregunta de selección multiple: -+-+-+-")
print ("¿Qué mostrará en pantalla el siguiente programa Python?")
def fred():
    print("Zap")
def jane():
    print("ABC")

print("-jane()")
print("-fred()")
print("-jane()")

print ("a) Zap ABC jane fred jane")
print ("b) Zap ABC Zap")
print ("c) ABC Zap jane")
print ("d) ABC Zap ABC")
print ("e) Zap Zap Zap")

print ("Escriba su respuesta:")
letra = input()
if letra == 'd':
    print ("Respuesta Correcta:", letra, ": ABC Zap ABC.")
else:
    print("¡No me lo creo!")