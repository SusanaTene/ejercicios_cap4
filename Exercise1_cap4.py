print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Ejercicio 1 del capitulo 5.')
print('Autora: Lilia Susana Tene. Mail: lilia.tene@unl.edu.ec')

#Ejercicio 1: Ejecuta el programa en tu sistema y observa qué números obtienes.

#La función random es solamente una de las muchas que trabajan con números
#aleatorios. La función randint toma los parámetros inferior y superior, y
#devuelve un entero entre inferior y superior (incluyendo ambos extremos).

import random

for numero in range(10):
    print("random.randint (5, 10)")
    print (random.randint(5,10))
#Para elegir un elemento de una secuencia aleatoriamente, se puede usar choice:
#    print(random.choice((14, 15, 20, 150)))
#t = [1, 2, 3]
# print (random.choice(t))

